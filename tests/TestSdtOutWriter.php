<?php

namespace asmaru\cli;

use asmaru\cli\writer\StdOutWriter;

class TestSdtOutWriter extends StdOutWriter {

	private string $buffer = '';

	protected function fwrite($s): void {
		$this->buffer .= $s;
	}

	public function setUseColors(bool $useColors): void {
		$this->useColors = $useColors;
	}

	public function getBuffer(): string {
		return $this->buffer;
	}

	public function clearBuffer(): void {
		$this->buffer = '';
	}
}