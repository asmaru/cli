<?php

namespace asmaru\cli;

use PHPUnit\Framework\TestCase;
use const PHP_EOL;

/**
 * @covers \asmaru\cli\StyledOutput
 */
class StyledOutputTest extends TestCase {

	public function testOutput() {

		$writer = new TestSdtOutWriter();
		$writer->setUseColors(false);

		$output = new StyledOutput($writer);

		$writer->clearBuffer();
		$output->write('write');
		$this->assertEquals('write', $writer->getBuffer());

		$writer->clearBuffer();
		$output->error('error');
		$this->assertEquals('error', $writer->getBuffer());

		$writer->clearBuffer();
		$output->warning('warning');
		$this->assertEquals('warning', $writer->getBuffer());

		$writer->clearBuffer();
		$output->info('info');
		$this->assertEquals('info', $writer->getBuffer());

		$writer->clearBuffer();
		$output->success('success');
		$this->assertEquals('success', $writer->getBuffer());

		$writer->clearBuffer();
		$output->writeLine('writeLine');
		$this->assertEquals('writeLine' . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->errorLine('errorLine');
		$this->assertEquals('errorLine' . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->warningLine('warningLine');
		$this->assertEquals('warningLine' . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->infoLine('infoLine');
		$this->assertEquals('infoLine' . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->successLine('successLine');
		$this->assertEquals('successLine' . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->newLine(2);
		$this->assertEquals(PHP_EOL . PHP_EOL, $writer->getBuffer());
	}

	public function testOutputWithColors() {

		$writer = new TestSdtOutWriter();
		$writer->setUseColors(true);

		$output = new StyledOutput($writer);

		$writer->clearBuffer();
		$output->error('error');
		$this->assertEquals("[0;31;49merror[0m", $writer->getBuffer());

		$writer->clearBuffer();
		$output->warning('warning');
		$this->assertEquals("[0;33;49mwarning[0m", $writer->getBuffer());

		$writer->clearBuffer();
		$output->info('info');
		$this->assertEquals("[0;34;49minfo[0m", $writer->getBuffer());

		$writer->clearBuffer();
		$output->success('success');
		$this->assertEquals("[0;97;42msuccess[0m", $writer->getBuffer());

		$writer->clearBuffer();
		$output->errorLine('errorLine');
		$this->assertEquals("[0;31;49merrorLine[0m" . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->warningLine('warningLine');
		$this->assertEquals("[0;33;49mwarningLine[0m" . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->infoLine('infoLine');
		$this->assertEquals("[0;34;49minfoLine[0m" . PHP_EOL, $writer->getBuffer());

		$writer->clearBuffer();
		$output->successLine('successLine');
		$this->assertEquals("[0;97;42msuccessLine[0m" . PHP_EOL, $writer->getBuffer());
	}
}