<?php

namespace asmaru\cli;

use asmaru\cli\style\ColorTheme;
use asmaru\cli\style\DefaultTheme;
use asmaru\cli\writer\WriterInterface;
use Closure;
use function count;
use function fgets;
use function intval;
use function max;
use function round;
use function str_pad;
use function str_repeat;
use function strlen;
use function strval;
use const PHP_EOL;
use const STDIN;
use const STR_PAD_LEFT;
use const STR_PAD_RIGHT;

class StyledOutput {

	private readonly DefaultTheme $colorTheme;

	public function __construct(private readonly WriterInterface $writer, ColorTheme $colorTheme = null) {
		$this->colorTheme = $colorTheme ?? new DefaultTheme();
	}

	public function error(string $message): void {
		$this->writer->write($message, $this->colorTheme->getErrorStyle());
	}

	public function warning(string $message): void {
		$this->writer->write($message, $this->colorTheme->getWarningStyle());
	}

	public function success(string $message): void {
		$this->writer->write($message, $this->colorTheme->getSuccessStyle());
	}

	public function info(string $message): void {
		$this->writer->write($message, $this->colorTheme->getInfoStyle());
	}

	public function errorLine(string $message): void {
		$this->error($message);
		$this->newLine();
	}

	public function warningLine(string $message): void {
		$this->warning($message);
		$this->newLine();
	}

	public function successLine(string $message): void {
		$this->success($message);
		$this->newLine();
	}

	public function infoLine(string $message): void {
		$this->info($message);
		$this->newLine();
	}

	public function write(string $message): void {
		$this->writer->write($message);
	}

	public function writeLine(string $message): void {
		$this->writer->write($message);
		$this->newLine();
	}

	public function newLine(int $count = 1): void {
		$this->writer->write(str_repeat(PHP_EOL, $count));
	}

	public function table(array $data, array $header = []): void {
		$columnWidths = [];
		$columnCount = count(!empty($header) ? $header : $data[0]);
		if (!empty($header)) {
			for ($i = 0; $i < $columnCount; $i++) {
				$columnWidths[$i] = strlen((string) $header[$i]);
			}
		}
		for ($i = 0; $i < $columnCount; $i++) {
			$max = 0;
			foreach ($data as $row) {
				$max = max(strlen((string) $row[$i]), $max);
			}
			$columnWidths[$i] = $max;
		}
		if (!empty($header)) {
			$this->writer->write('|');
			for ($i = 0; $i < $columnCount; $i++) {
				$this->writer->write(str_pad((string) $header[$i], $columnWidths[$i], ' '));
				$this->writer->write('|');
			}
			$this->newLine();
		}
		foreach ($data as $row) {
			$this->writer->write('|');
			for ($i = 0; $i < $columnCount; $i++) {
				$this->writer->write(str_pad((string) $row[$i], $columnWidths[$i], ' '));
				$this->writer->write('|');
			}
			$this->newLine();
		}
	}

	public function progress(int $max, int $step): void {
		$width = 20;
		$percent = strval($step >= $max ? 100 : round($step / ($max / 100)));
		$finished = intval($step > 0 ? round($width / ($max / $step)) : 0);
		$this->writer->write("\r");
		$this->info(str_pad($percent, 3, ' ', STR_PAD_LEFT) . '%');
		$this->writer->write('[' . str_pad(str_repeat('#', $finished) . '#', $width + 1, ' ', STR_PAD_RIGHT) . ']');
	}

	public function dialog(string $message, DialogOption ...$options): void {
		$i = 1;
		$this->newLine();
		$this->infoLine($message);
		$this->newLine();
		foreach ($options as $option) {
			$this->info(strval($i));
			$this->writer->write($option->getLabel());
			$i++;
		}
		$this->writer->write(':');
		$value = (int)fgets(STDIN);
		$index = $value - 1;
		if (isset($options[$index])) {
			$this->newLine();
			$options[$index]->execute($this);
		}
	}

	public function dialogOption(string $message, ?Closure $callback): DialogOption {
		return new DialogOption($message, $callback);
	}

	public function clear(): void {
		$this->writer->write("\e[H\e[J");
	}
}