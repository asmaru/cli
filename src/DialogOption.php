<?php

declare(strict_types=1);

namespace asmaru\cli;

use Closure;

class DialogOption {

	public function __construct(
		private readonly string   $label,
		private readonly ?Closure $callback
	) {
	}

	public function getLabel(): string {
		return $this->label;
	}

	public function execute(StyledOutput $output): void {
		$this->callback?->__invoke($output);
	}
}