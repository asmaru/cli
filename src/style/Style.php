<?php

namespace asmaru\cli\style;

class Style {

	public function __construct(
		private readonly TextColor       $textColor,
		private readonly BackgroundColor $backgroundColor,
		private readonly bool            $bold = false) {
	}

	public function getTextColor(): TextColor {
		return $this->textColor;
	}

	public function getBackgroundColor(): BackgroundColor {
		return $this->backgroundColor;
	}

	public function isBold(): bool {
		return $this->bold;
	}
}