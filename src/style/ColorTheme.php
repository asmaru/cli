<?php

namespace asmaru\cli\style;

interface ColorTheme {

	public function getDefaultStyle(): Style;

	public function getInfoStyle(): Style;

	public function getPadding();

	public function getWarningStyle(): Style;

	public function getErrorStyle(): Style;

	public function getSuccessStyle(): Style;
}