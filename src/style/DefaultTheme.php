<?php

namespace asmaru\cli\style;

class DefaultTheme implements ColorTheme {

	public function getDefaultStyle(): Style {
		return new Style(TextColor::DEFAULT, BackgroundColor::DEFAULT, false);
	}

	public function getInfoStyle(): Style {
		return new Style(TextColor::BLUE, BackgroundColor::DEFAULT, false);
	}

	public function getPadding() {
		return 0;
	}

	public function getWarningStyle(): Style {
		return new Style(TextColor::YELLOW, BackgroundColor::DEFAULT, false);
	}

	public function getErrorStyle(): Style {
		return new Style(TextColor::RED, BackgroundColor::DEFAULT, false);
	}

	public function getSuccessStyle(): Style {
		return new Style(TextColor::WHITE, BackgroundColor::GREEN, false);
	}
}