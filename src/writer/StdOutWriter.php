<?php

declare(strict_types=1);

namespace asmaru\cli\writer;

use asmaru\cli\style\Style;
use function function_exists;
use function fwrite;
use function getenv;
use function posix_isatty;
use const DIRECTORY_SEPARATOR;
use const STDOUT;

class StdOutWriter implements WriterInterface {

	protected bool $useColors;

	public function __construct() {
		$this->useColors = $this->hasColorSupport();
	}

	public function write(string $message, Style $style = null) {

		$resetColor = false;

		if ($this->useColors && $style !== null) {
			$this->fwrite(sprintf("\e[%s;%s;%sm", $style->isBold() ? '1' : '0', $style->getTextColor()->value, $style->getBackgroundColor()->value));
			$resetColor = true;
		}

		$this->fwrite($message);

		if ($resetColor) {
			$this->fwrite("\e[0m");
		}
	}

	protected function fwrite($s): void {
		fwrite(STDOUT, (string) $s);
	}

	/**
  * @link https://github.com/symfony/Console/blob/master/Output/StreamOutput.php#L95-L102
  */
 protected function hasColorSupport(): bool {
		if (DIRECTORY_SEPARATOR === '\\') {
			return getenv('ANSICON') !== false || getenv('ConEmuANSI') === 'ON' || getenv('TERM') === 'xterm';
		}
		return function_exists('posix_isatty') && @posix_isatty(STDOUT);
	}
}