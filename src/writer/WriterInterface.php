<?php

declare(strict_types=1);

namespace asmaru\cli\writer;

use asmaru\cli\style\Style;

interface WriterInterface {

	public function write(string $message, Style $style = null);
}